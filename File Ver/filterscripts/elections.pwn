#include <a_samp>
#include <streamer>
#include <mxini>

/*��������*/

#define ACCOUNTS_DIRECTORY "../scriptfiles" // ���� � ��������(�����) � ����������

const MAX_CANDIDATES = 10; // ������������ ���������� ���������� (�� ����� 20) | ��� ������ �����, ��� ������ ������ ����� ��������.
const SERVER_SLOTS = 1000; // ���������� ������ �������

/*���������� ����������*/
static Candidates[MAX_CANDIDATES][MAX_PLAYER_NAME+7], // ����� ����������
	Sphere[3],
	HelpPic;
new bool: Voting[SERVER_SLOTS+1]; // �������� �� ������� ���������
new bool: Voted[SERVER_SLOTS+1]; // �������� �� ��������� �����������
new Votes[MAX_CANDIDATES], // ���������� ������� ��� ������� ���������
	NumberOfCandidates, // ���������� ���������� (������� �� ����� candidates.ini)
	String[128];

public OnFilterScriptInit()
{
  	if(!fexist("candidates.ini")) // �������� �� ������� ����� candidates.ini � ����� scriptfiles
	{
		new iFile = ini_createFile("candidates.ini");
		ini_openFile("candidates.ini");
		ini_setString(iFile, "������ ����������", " ");
		ini_closeFile(iFile);
	}
	/*�������� ���������� �� ����� candidates.ini*/
  	new File = ini_openFile("candidates.ini");
  	if(File) print("ELECTIONS SCRIPT: ������ ��� �������� ����� 'candidates.ini'"); // ���� �� ������� ������� ���� candidates.ini
	new temp_string[9];
	for(new i = 0; i < MAX_CANDIDATES; i++)
	{
	    format(temp_string, sizeof temp_string, "Cand%d", i+1);
	    if(ini_getString(File, temp_string, Candidates[i]) == INI_KEY_NOT_FOUND) break;
	    new startbracket = strfind(Candidates[i], "(", true),
			endbracket = strfind(Candidates[i], ")", true);
		strmid(temp_string, Candidates[i], startbracket+1, endbracket);
		Votes[i] = strval(temp_string);
		strdel(Candidates[i], startbracket, endbracket);
		printf("�������� %d �������� (���: %s | ������: %d)", i+1, Candidates[i], Votes[i]);
	}
	ini_closeFile(File);
	/*����� �������� ���������� �� ����� candidates.ini*/
	Sphere[0] = CreateDynamicSphere(363.3222,201.5887,1008.3828, 0.5); // ����� ��� ���� �1
	Sphere[1] = CreateDynamicSphere(361.1423,201.5883,1008.3828, 0.5); // ����� ��� ���� �2
	Sphere[2] = CreateDynamicSphere(358.9418,201.5882,1008.3828, 0.5); // ����� ��� ���� �3
	CreateObject(3862, 356.562744, 211.871612, 1008.382812, 0.000000, 0.000000, 0.000000); // �������
	CreateObject(2446, 363.3, 201.0, 1007.33, 0.000000, 0.000000, 0.000000); // ���� �1
	CreateObject(2446, 361.1, 201.0, 1007.33, 0.000000, 0.000000, 0.000000); // ���� �2
	CreateObject(2446, 358.9, 201.0, 1007.33, 0.000000, 0.000000, 0.000000); // ���� �3
	HelpPic = CreateDynamicPickup(1239, 23, 361.3123, 210.6893, 1008.3828); // ����� ������
	/*3D �����*/
	CreateDynamic3DTextLabel("������������� ���������\n(/vote)",0x9ACD32AA,356.5287,210.1769,1008.3828,15.0);
	CreateDynamic3DTextLabel("���� �1",0x9ACD32AA,363.3222,201.5887,1008.3828,15.0);
	CreateDynamic3DTextLabel("���� �2",0x9ACD32AA,361.1423,201.5883,1008.3828,15.0);
	CreateDynamic3DTextLabel("���� �3",0x9ACD32AA,358.9418,201.5882,1008.3828,15.0);
	CreateDynamic3DTextLabel("������������� �������",0x9ACD32AA,366.5539,189.0544,1008.3828,20.0);
	return 1;
}

public OnFilterScriptExit()
{
	return 1;
}

public OnPlayerCommandText(playerid, cmdtext[])
{
	if(!strcmp(cmdtext, "/vote", true) && IsPlayerInRangeOfPoint(playerid,2.0,356.5287,210.1769,1008.3828))
	{
		if(Voted[playerid]) return SendClientMessage(playerid, 0xAFAFAFAA, "�� ��� ����������");
		ApplyAnimation(playerid,"CRIB","CRIB_Use_Switch",4.0,0,0,0,0,0);
		SetPlayerAttachedObject(playerid, 8, 1277, 5, 0.25, 0, 0, 0, 0, 0);
		SendClientMessage(playerid, -1, "��������� ������������� ��������� � ���� �� ���");
		Voting[playerid] = true;
	}
	if(!strcmp(cmdtext, "/elections", true))
	{
    	if(!IsPlayerAdmin(playerid)) return 1;
    	ShowPlayerDialog(playerid, 9217, DIALOG_STYLE_LIST, "{ECCC15}���������� ��������", "1. ������ ����������\n2. �������� ���������", "�������", "�������");
	}
	return 0;
}

public OnPlayerConnect(playerid)
{
    new Name[MAX_PLAYER_NAME];
   	GetPlayerName(playerid, Name, MAX_PLAYER_NAME);
	format(String, sizeof String, "%s/%s.ini", ACCOUNTS_DIRECTORY, Name);
	if(fexist(String))
	{
    	new File = ini_openFile(String);
    	new voted;
    	ini_getInteger(File, "Voted", voted);
    	Voted[playerid] = voted ? true : false;
    	ini_closeFile(File);
    }
	RemoveBuildingForPlayer(playerid, 1998, 354.3828, 200.9766, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 355.5703, 200.7656, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 357.6719, 200.7656, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 354.7188, 204.5234, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2009, 354.4297, 209.3828, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 354.7109, 212.0625, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2008, 355.3984, 202.9922, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 355.5156, 208.6172, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1999, 355.4453, 211.4063, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2008, 356.4688, 201.9766, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 356.5938, 203.4219, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1998, 357.4844, 203.9922, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2001, 357.5625, 204.7969, 1007.3594, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 356.3828, 213.5938, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1999, 356.4688, 210.3828, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 357.6953, 209.5938, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2009, 357.4844, 212.4063, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2001, 358.1641, 211.0234, 1007.3750, 0.25);
	RemoveBuildingForPlayer(playerid, 2001, 359.5703, 202.8125, 1007.3750, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 360.0156, 204.1328, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2009, 360.2734, 201.2969, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1998, 360.7734, 209.7344, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2001, 360.8203, 209.0078, 1007.3594, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 361.2344, 201.5313, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1999, 362.3125, 202.2969, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1999, 361.2813, 203.3203, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 362.2656, 204.1328, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 361.7500, 209.8281, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2008, 362.8594, 210.7344, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 362.9219, 200.6953, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2009, 363.3203, 204.3203, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 363.0625, 212.3906, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 363.9063, 209.8281, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1998, 363.8672, 212.7500, 1007.3672, 0.25);
	return 1;
}

public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	if(dialogid == 9215 && response)
	{
	    SetPVarInt(playerid, "Vote", listitem);
	    format(String, sizeof(String), "{FFFFFF}�� ������� ��� ������ ������������� �� %s?", Candidates[listitem]);
	    ShowPlayerDialog(playerid, 9216, DIALOG_STYLE_MSGBOX, "{ECCC15}�����������", String, "��", "���");
	}
	else if(dialogid == 9216)
	{
	    if(!response)
	    {
	        new String_[MAX_CANDIDATES*(MAX_PLAYER_NAME+36)];
	    	for(new i; i<NumberOfCandidates; i++)
	    	{
	        	format(String, sizeof(String), "�������� �%d: %s (%d �������)\n", i+1, Candidates[i], Votes[i]);
	        	strcat(String_, String);
			}
			ShowPlayerDialog(playerid, 9215, DIALOG_STYLE_LIST, "{ECCC15}�� ���� �� ������ �������������?", String_, "����������", "������");
	    }
	    else
	    {
	        ApplyAnimation(playerid,"CRIB","CRIB_Use_Switch",4.0,0,0,0,0,0);
			RemovePlayerAttachedObject(playerid, 8);
			Voting[playerid] = false;
			format(String, sizeof(String), "�� ������ ���� ����� �� %s", Candidates[GetPVarInt(playerid, "Vote")]);
			SendClientMessage(playerid, -1, String);
			Voted[playerid] = true;
	        new Name[MAX_PLAYER_NAME];
	        GetPlayerName(playerid, Name, MAX_PLAYER_NAME);
	        format(String, sizeof String, "%s/%s.ini", ACCOUNTS_DIRECTORY, Name);
    		new File = ini_openFile(String);
    		ini_setInteger(File, "Voted", 1);
    		ini_closeFile(File);
			Votes[GetPVarInt(playerid, "Vote")] ++;
			SaveCandidate(GetPVarInt(playerid, "Vote"));
	    }
	}
	else if(dialogid == 9217)
	{
	    if(!response) return 1;
	    if(!listitem)
	    {
	        new String_[MAX_CANDIDATES*(MAX_PLAYER_NAME+25)];
   			for(new i; i<NumberOfCandidates; i++)
    		{
    			format(String, sizeof(String), "%d. %s (%d �������)\n", i+1, Candidates[i], Votes[i]);
     			strcat(String_, String);
			}
			ShowPlayerDialog(playerid, 9218, DIALOG_STYLE_LIST, "{ECCC15}������ ����������", String_, "�����", "�����");
	    }
	    else if(listitem == 1) ShowPlayerDialog(playerid, 9220, DIALOG_STYLE_INPUT, "{ECCC15}���������� ���������", "{FFFFFF}������� ��� ���������:", "��������", "�����");
	}
	else if(dialogid == 9218)
	{
	    if(!response) return OnPlayerCommandText(playerid, "/elections");
	    format(String, sizeof(String), "{ECCC15}�������� %s", Candidates[listitem]);
	    SetPVarInt(playerid, "SelectCandidate", listitem);
	    ShowPlayerDialog(playerid, 9219, DIALOG_STYLE_LIST, String, "1. �������", "�������", "�����");
	}
	else if(dialogid == 9219)
	{
	    OnPlayerCommandText(playerid, "/elections");
	    if(!response) return 1;
	    if(NumberOfCandidates <= 1)
	    {
	        SendClientMessage(playerid, -1, "������ �������� ������ ������ ���������");
	        return 1;
	    }
		if(!listitem)
		{
		    DeleteCandidate(GetPVarInt(playerid, "SelectCandidate"));
		    SendClientMessage(playerid, -1, "�������� �����");
		}
	}
	else if(dialogid == 9220)
	{
	    OnPlayerCommandText(playerid, "/elections");
	    if(!response) return 1;
	    if(NumberOfCandidates >= MAX_CANDIDATES)
	    {
	        SendClientMessage(playerid, -1, "������ �������� ������ ����������");
	        return 1;
	    }
	    format(Candidates[NumberOfCandidates++], MAX_PLAYER_NAME, inputtext);
	    format(String, sizeof String, "Cand%d", NumberOfCandidates);
	    new File = ini_openFile("candidates.ini");
	    new temp_string[MAX_PLAYER_NAME+7];
	    format(temp_string, sizeof temp_string, "%s(0)", Candidates[NumberOfCandidates-1]);
	    ini_setString(File, String, temp_string);
	    ini_closeFile(File);
	    SendClientMessage(playerid, -1, "�������� ��������");
	}
	return 0;
}

public OnPlayerEnterDynamicArea(playerid, areaid)
{
	if(areaid >= Sphere[0] && areaid <= Sphere[2] && Voting[playerid])
	{
	   	new String_[MAX_CANDIDATES*(MAX_PLAYER_NAME+36)];
   		for(new i; i<NumberOfCandidates; i++)
    	{
    		format(String, sizeof(String), "�������� �%d: %s (%d �������)\n", i+1, Candidates[i], Votes[i]);
     		strcat(String_, String);
		}
		ShowPlayerDialog(playerid, 9215, DIALOG_STYLE_LIST, "{ECCC15}�� ���� �� ������ �������������?", String_, "����������", "������");
	}
	return 1;
}

public OnPlayerPickUpDynamicPickup(playerid, pickupid)
{
	if(pickupid == HelpPic)
	{
		new String_[(MAX_PLAYER_NAME+26)*MAX_CANDIDATES+140];
		format(String_, sizeof(String_), "{FFFFFF}�� ���������� � ������������� �������, �����\n������������� �� ������ �� ���������� ��������\n������������� ��������� � ��������� ��� � ���� �� ���.\n\n������ ����������:");
		for(new i; i<NumberOfCandidates; i++)
	    {
	        format(String, sizeof(String), "\n%d. %s (%d �������)", i+1, Candidates[i], Votes[i]);
	        strcat(String_, String);
		}
		ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, "{ECCC15}������������� �������", String_, "�������", "");
	}
	return 1;
}

stock DeleteCandidate(candidate) // �������� ���������
{
	format(Candidates[candidate], MAX_PLAYER_NAME, Candidates[--NumberOfCandidates]);
	Votes[candidate] = Votes[NumberOfCandidates];
	new temp_string[MAX_PLAYER_NAME+7];
	format(String, sizeof String, "Cand%d", candidate+1);
	new File = ini_openFile("candidates.ini");
	format(temp_string, sizeof temp_string, "%s(%d)", Candidates[NumberOfCandidates], Votes[NumberOfCandidates]);
	ini_setString(File, String, temp_string);
	format(temp_string, sizeof temp_string, "Cand%d", NumberOfCandidates+1);
	ini_removeKey(File, temp_string);
	ini_closeFile(File);
}

stock SaveCandidate(candidate) // ���������� ���������
{
	new temp_string[9];
	format(temp_string, sizeof temp_string, "Cand%d", candidate+1);
	format(String, sizeof String, "%s(%d)", Candidates[candidate], Votes[candidate]);
	new File = ini_openFile("candidates.ini");
	ini_setString(File, temp_string, String);
	ini_closeFile(File);
}
