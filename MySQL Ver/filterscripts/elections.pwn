#include <a_samp>
#include <a_mysql>
#include <streamer>

/*��������*/

const MAX_CANDIDATES = 10; // ������������ ���������� ���������� (�� ����� 20) | ��� ������ �����, ��� ������ ������ ����� ��������.
const SERVER_SLOTS = 1000; // ���������� ������ �������
static const SQL_INFO[][] = {"����", "������������", "����", "������"}; // ������ ����������� � SQL ������� (���� ������)

/*���������� ����������*/

static Candidates[MAX_CANDIDATES][MAX_PLAYER_NAME], // ����� ����������
	CH, // SQL ����������
	Sphere[3],
	HelpPic;
new bool: Voting[SERVER_SLOTS+1]; // �������� �� ������� ���������
new Votes[MAX_CANDIDATES], // ���������� ������� ��� ������� ���������
	NumberOfCandidates, // ���������� ���������� (������� �� ���� ������)
	String[128];

public OnFilterScriptInit()
{
	CH = mysql_connect(SQL_INFO[0], SQL_INFO[1], SQL_INFO[2], SQL_INFO[3]); // ����������� � ���� ������
	/*�������� ����������� ������ � ���� ������*/
	mysql_tquery(CH, "CREATE TABLE IF NOT EXISTS `candidates` (\
  	`Name` varchar(24) NOT NULL,\
  	`Votes` int(11) NOT NULL DEFAULT '0')");
    mysql_tquery(CH, "CREATE TABLE IF NOT EXISTS `votes` (\
  	`Name` varchar(24) NOT NULL,\
  	`Voted` varchar(24) NOT NULL DEFAULT '0')");
	mysql_tquery(CH, "SELECT * FROM `candidates`", "LoadCandidates"); // �������� ���������� � ���������� �� ���� ������
	Sphere[0] = CreateDynamicSphere(363.3222,201.5887,1008.3828, 0.5); // ����� ��� ���� �1
	Sphere[1] = CreateDynamicSphere(361.1423,201.5883,1008.3828, 0.5); // ����� ��� ���� �2
	Sphere[2] = CreateDynamicSphere(358.9418,201.5882,1008.3828, 0.5); // ����� ��� ���� �3
	CreateDynamicObject(3862, 356.562744, 211.871612, 1008.382812, 0.000000, 0.000000, 0.000000); // �������
	CreateDynamicObject(2446, 363.3, 201.0, 1007.33, 0.000000, 0.000000, 0.000000); // ���� �1
	CreateDynamicObject(2446, 361.1, 201.0, 1007.33, 0.000000, 0.000000, 0.000000); // ���� �2
	CreateDynamicObject(2446, 358.9, 201.0, 1007.33, 0.000000, 0.000000, 0.000000); // ���� �3
	HelpPic = CreateDynamicPickup(1239, 23, 361.3123, 210.6893, 1008.3828); // ����� ������
	/*3D �����*/
	CreateDynamic3DTextLabel("������������� ���������\n(/vote)",0x9ACD32AA,356.5287,210.1769,1008.3828,15.0);
	CreateDynamic3DTextLabel("���� �1",0x9ACD32AA,363.3222,201.5887,1008.3828,15.0);
	CreateDynamic3DTextLabel("���� �2",0x9ACD32AA,361.1423,201.5883,1008.3828,15.0);
	CreateDynamic3DTextLabel("���� �3",0x9ACD32AA,358.9418,201.5882,1008.3828,15.0);
	CreateDynamic3DTextLabel("������������� �������",0x9ACD32AA,366.5539,189.0544,1008.3828,20.0);
	return 1;
}

public OnFilterScriptExit()
{
	return 1;
}

public OnPlayerCommandText(playerid, cmdtext[])
{
	if(!strcmp(cmdtext, "/vote", true) && IsPlayerInRangeOfPoint(playerid,2.0,356.5287,210.1769,1008.3828))
	{
    	new Name[MAX_PLAYER_NAME];
    	GetPlayerName(playerid, Name, MAX_PLAYER_NAME);
		mysql_format(CH, String, sizeof(String), "SELECT * FROM `votes` WHERE `Name` = '%e'", Name);
		mysql_tquery(CH, String, "OnPlayerVoting", "d", playerid);
	}
	if(!strcmp(cmdtext, "/elections", true))
	{
    	if(!IsPlayerAdmin(playerid)) return 1;
    	ShowPlayerDialog(playerid, 9217, DIALOG_STYLE_LIST, "{ECCC15}���������� ��������", "1. ������ ����������\n2. �������� ���������", "�������", "�������");
	}
	return 0;
}

public OnPlayerConnect(playerid)
{
	Voting[playerid] = false;
	RemoveBuildingForPlayer(playerid, 1998, 354.3828, 200.9766, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 355.5703, 200.7656, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 357.6719, 200.7656, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 354.7188, 204.5234, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2009, 354.4297, 209.3828, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 354.7109, 212.0625, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2008, 355.3984, 202.9922, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 355.5156, 208.6172, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1999, 355.4453, 211.4063, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2008, 356.4688, 201.9766, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 356.5938, 203.4219, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1998, 357.4844, 203.9922, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2001, 357.5625, 204.7969, 1007.3594, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 356.3828, 213.5938, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1999, 356.4688, 210.3828, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 357.6953, 209.5938, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2009, 357.4844, 212.4063, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2001, 358.1641, 211.0234, 1007.3750, 0.25);
	RemoveBuildingForPlayer(playerid, 2001, 359.5703, 202.8125, 1007.3750, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 360.0156, 204.1328, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2009, 360.2734, 201.2969, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1998, 360.7734, 209.7344, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2001, 360.8203, 209.0078, 1007.3594, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 361.2344, 201.5313, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1999, 362.3125, 202.2969, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1999, 361.2813, 203.3203, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 362.2656, 204.1328, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 361.7500, 209.8281, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2008, 362.8594, 210.7344, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 362.9219, 200.6953, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 2009, 363.3203, 204.3203, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 363.0625, 212.3906, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1806, 363.9063, 209.8281, 1007.3672, 0.25);
	RemoveBuildingForPlayer(playerid, 1998, 363.8672, 212.7500, 1007.3672, 0.25);
	return 1;
}

public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	if(dialogid == 9215 && response)
	{
	    SetPVarInt(playerid, "Vote", listitem);
	    format(String, sizeof(String), "{FFFFFF}�� ������� ��� ������ ������������� �� %s?", Candidates[listitem]);
	    ShowPlayerDialog(playerid, 9216, DIALOG_STYLE_MSGBOX, "{ECCC15}�����������", String, "��", "���");
	}
	else if(dialogid == 9216)
	{
	    if(!response)
	    {
	        new String_[MAX_CANDIDATES*(MAX_PLAYER_NAME+36)];
	    	for(new i; i<NumberOfCandidates; i++)
	    	{
	        	format(String, sizeof(String), "�������� �%d: %s (%d �������)\n", i+1, Candidates[i], Votes[i]);
	        	strcat(String_, String);
			}
			ShowPlayerDialog(playerid, 9215, DIALOG_STYLE_LIST, "{ECCC15}�� ���� �� ������ �������������?", String_, "����������", "������");
	    }
	    else
	    {
	        ApplyAnimation(playerid, "CRIB", "CRIB_Use_Switch", 4.0, 0, 0, 0, 0, 0);
			RemovePlayerAttachedObject(playerid, 8);
			Voting[playerid] = false;
			format(String, sizeof(String), "�� ������ ���� ����� �� %s", Candidates[GetPVarInt(playerid, "Vote")]);
			SendClientMessage(playerid, -1, String);
	        new Name[MAX_PLAYER_NAME];
	        GetPlayerName(playerid, Name, MAX_PLAYER_NAME);
	        mysql_format(CH, String, sizeof(String), "INSERT INTO `votes` (`Name`,`Voted`) VALUES ('%e','%e')", Name, Candidates[GetPVarInt(playerid, "Vote")]);
			mysql_tquery(CH, String);
			Votes[GetPVarInt(playerid, "Vote")] ++;
			SaveCandidate(GetPVarInt(playerid, "Vote"));
	    }
	}
	else if(dialogid == 9217)
	{
	    if(!response) return 1;
	    if(!listitem)
	    {
	        new String_[MAX_CANDIDATES*(MAX_PLAYER_NAME+25)];
   			for(new i; i<NumberOfCandidates; i++)
    		{
    			format(String, sizeof(String), "%d. %s (%d �������)\n", i+1, Candidates[i], Votes[i]);
     			strcat(String_, String);
			}
			ShowPlayerDialog(playerid, 9218, DIALOG_STYLE_LIST, "{ECCC15}������ ����������", String_, "�����", "�����");
	    }
	    else if(listitem == 1) ShowPlayerDialog(playerid, 9220, DIALOG_STYLE_INPUT, "{ECCC15}���������� ���������", "{FFFFFF}������� ��� ���������:", "��������", "�����");
	}
	else if(dialogid == 9218)
	{
	    if(!response) return OnPlayerCommandText(playerid, "/elections");
	    format(String, sizeof(String), "{ECCC15}�������� %s", Candidates[listitem]);
	    SetPVarInt(playerid, "SelectCandidate", listitem);
	    ShowPlayerDialog(playerid, 9219, DIALOG_STYLE_LIST, String, "1. �������", "�������", "�����");
	}
	else if(dialogid == 9219)
	{
	    OnPlayerCommandText(playerid, "/elections");
	    if(!response) return 1;
	    if(NumberOfCandidates <= 1)
	    {
	        SendClientMessage(playerid, -1, "������ �������� ������ ������ ���������");
	        return 1;
	    }
		if(!listitem)
		{
		    DeleteCandidate(GetPVarInt(playerid, "SelectCandidate"));
		    SendClientMessage(playerid, -1, "�������� �����");
		}
	}
	else if(dialogid == 9220)
	{
	    OnPlayerCommandText(playerid, "/elections");
	    if(!response) return 1;
	    if(NumberOfCandidates >= MAX_CANDIDATES)
	    {
	        SendClientMessage(playerid, -1, "������ �������� ������ ����������");
	        return 1;
	    }
	    format(Candidates[NumberOfCandidates++], MAX_PLAYER_NAME, inputtext);
		mysql_format(CH, String, sizeof(String), "INSERT INTO `candidates` (`Name`) VALUES ('%e')", inputtext);
		mysql_tquery(CH, String);
	    SendClientMessage(playerid, -1, "�������� ��������");
	}
	return 0;
}

public OnPlayerEnterDynamicArea(playerid, areaid)
{
	if(areaid >= Sphere[0] && areaid <= Sphere[2] && Voting[playerid])
	{
	   	new String_[MAX_CANDIDATES*(MAX_PLAYER_NAME+36)];
   		for(new i; i<NumberOfCandidates; i++)
    	{
    		format(String, sizeof(String), "�������� �%d: %s (%d �������)\n", i+1, Candidates[i], Votes[i]);
     		strcat(String_, String);
		}
		ShowPlayerDialog(playerid, 9215, DIALOG_STYLE_LIST, "{ECCC15}�� ���� �� ������ �������������?", String_, "����������", "������");
	}
	return 1;
}

public OnPlayerPickUpDynamicPickup(playerid, pickupid)
{
	if(pickupid == HelpPic)
	{
		new String_[(MAX_PLAYER_NAME+26)*MAX_CANDIDATES+140];
		format(String_, sizeof(String_), "{FFFFFF}�� ���������� � ������������� �������, �����\n������������� �� ������ �� ���������� ��������\n������������� ��������� � ��������� ��� � ���� �� ���.\n\n������ ����������:");
		for(new i; i<NumberOfCandidates; i++)
	    {
	        format(String, sizeof(String), "\n%d. %s (%d �������)", i+1, Candidates[i], Votes[i]);
	        strcat(String_, String);
		}
		ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, "{ECCC15}������������� �������", String_, "�������", "");
	}
	return 1;
}

/*���������� ��� �������� ����� ������� /vote*/
forward OnPlayerVoting(playerid);
public OnPlayerVoting(playerid)
{
	if(cache_get_row_count(CH)) return SendClientMessage(playerid, 0xAFAFAFAA, "�� ��� ����������");
	ApplyAnimation(playerid, "CRIB", "CRIB_Use_Switch", 4.0, 0, 0, 0, 0, 0);
	SetPlayerAttachedObject(playerid, 8, 1277, 5, 0.25, 0, 0, 0, 0, 0);
	SendClientMessage(playerid, -1, "��������� ������������� ��������� � ���� �� ���");
	Voting[playerid] = true;
	return 1;
}

/*�������� ����������*/
forward LoadCandidates();
public LoadCandidates()
{
	NumberOfCandidates = cache_get_row_count(CH);
	if(NumberOfCandidates > MAX_CANDIDATES) NumberOfCandidates = MAX_CANDIDATES;
	for(new i; i<NumberOfCandidates; i++)
	{
	    cache_get_field_content(i, "Name", Candidates[i], CH, sizeof(Candidates[]));
	    Votes[i] = cache_get_field_content_int(i, "Votes", CH);
	}
	
}

stock DeleteCandidate(candidate) // �������� ���������
{
    mysql_format(CH, String, sizeof(String), "DELETE FROM `candidates` WHERE `Name` = '%e'", Candidates[candidate]);
	mysql_tquery(CH, String);
	format(Candidates[candidate], MAX_PLAYER_NAME, Candidates[--NumberOfCandidates]);
	Votes[candidate] = Votes[NumberOfCandidates];
}

stock SaveCandidate(candidate) // ���������� ���������
{
	mysql_format(CH, String, sizeof(String), "UPDATE `candidates` SET `Votes` = '%d' WHERE `Name` = '%e'", Votes[candidate], Candidates[candidate]);
	mysql_tquery(CH, String);
}
